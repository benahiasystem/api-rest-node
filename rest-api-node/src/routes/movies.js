const { Router } = require('express');
const router = Router();
const _ = require('underscore');

const movies = require('../sample.json'); 
//console.log(movies);

router.get('/', (req, res) =>{
    res.json(movies);
});

router.post('/', (req, res) => {
    const {title,director,year,rating } = req.body;

    if(title && director && year && rating){
        const id = movies.length + 1;
        const newMovie = {id, ...req.body};
        movies.push(newMovie);      
        res.json(movies);
    }else{
        res.status(500).json({error:'There was an error.'});
    }
});

router.put('/:id', (req, res) => {
    const { id } = req.params;
    const {title,director,year,rating } = req.body;

    if (title && director && year && rating){
        _.each(movies, (movies, i) => {
            try {
                if (movies.id == id) {
                    movies.id       = id;
                    movies.title    = title;
                    movies.director = director;
                    movies.year     = year;
                    movies.rating   = rating;
                }
            } catch (error) {
                console.log('hubo un error', error)
            }
        });
     } else{
        res.status(500).json({error:'There was a error'});
        }
    res.json(movies);
});

router.delete('/:id', (req, res) => {
    const  { id }  = req.params;
     _.each(movies, (movies, i) => {
        try {
            if (movies.id == id){
                remove(i);
            }   
        } catch (error) {
            //console.log('hubo un error', error)
        }
     });

        function remove(i) { 
            movies.splice(i, 1);
        } 
     res.send(movies);
});

module.exports = router;
